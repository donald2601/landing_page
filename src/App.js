import './App.css';
import Footer from './components/Footer';
import Navbar from './components/Navbar';
import Body from './components/Body';
import './components/index.css'

function App() {
  return (
    <div className="App" >
     <Navbar/>
     <Body/>
     <Footer/>
    </div>
  );
}

export default App;
