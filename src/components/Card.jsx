import React from 'react'
import iconSecurty from '../assets/images/icon-security.svg'
import iconCollaboration from '../assets/images/icon-collaboration.svg'
import iconFile from '../assets/images/icon-any-file.svg'
import iconAccess from '../assets/images/icon-access-anywhere.svg'



export default function Card() {
  return (
    <div className='d-flex text-center flex-wrap m-3 p-3 justify-content-center'>
        <div className='d-flex flex-wrap justify-content-between' style={{width : '80%' }}>
        <div class="card d-flex justify-content-center align-items-center mb-3" style={{width: "11rem",border : 0 ,  backgroundColor: 'hsl(218, 28%, 13%)'}}>
            <img src={iconAccess} class="card-img-top" alt="security" style={{width : 100}}/>
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>

        <div class="card d-flex justify-content-center align-items-center mb-3" style={{width: "11rem" , border : 0, backgroundColor: 'hsl(218, 28%, 13%)'}}>
            <img src={iconSecurty} class="card-img-top" alt="security" style={{width : 100}}/>
            <div class="card-body" >
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>

        <div class="card d-flex justify-content-center align-items-center mb-3" style={{width: "11rem" , border : 0, backgroundColor: 'hsl(218, 28%, 13%)'}}>
            <img src={iconCollaboration} class="card-img-top" alt="security" style={{width : 100}}/>
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>

        <div class="card d-flex justify-content-center align-items-center mb-3" style={{width: "11rem" , border : 0, backgroundColor: 'hsl(218, 28%, 13%)'}}>
            <img src={iconFile} class="card-img-top" alt="security" style={{width : 100}}/>
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>
        </div>
    </div>
  )
}
