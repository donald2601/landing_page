import React from 'react'
import bgQuote from '../assets/images/bg-quotes.png'

export default function Card2() {
  return (
    <div>
        <div className='d-flex text-center flex-wrap m-3 p-3 justify-content-center'>
        <div className='d-flex flex-wrap justify-content-around' style={{width : '90%' }}>
        <p id='bgQuote'> <img src={bgQuote} alt="bg" /> </p>
        <div class="card d-flex justify-content-center align-items-center mb-3" style={{backgroundColor : 'hsl(219, 30%, 18%)' , width : 350 , height: 140}}>
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>

        <div class="card d-flex justify-content-center align-items-center mb-3" style={{backgroundColor : 'hsl(219, 30%, 18%)' , width : 350 , height: 140}}>
            <div class="card-body" >
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>

        <div class="card d-flex justify-content-center align-items-center mb-3" style={{backgroundColor : 'hsl(219, 30%, 18%)' , width : 350 , height: 140}}>
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
        </div>
        </div>
    </div>
    </div>
  )
}
