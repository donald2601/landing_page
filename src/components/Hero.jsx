import React from 'react';
import illustration from '../assets/images/illustration-intro.png';

export default function Heros() {
  return (
    <div>
        <div class="px-5 py-5 my-5 text-center">
            <img class="d-block mx-auto mb-4" src={illustration} alt="" width="400" height="300"/>
            <h1 class="display-5 fw-bold"  style={{color : 'hsl(0, 0%, 100%)'}}> All you file in one secure location, accecible anywhere .</h1>
        <div class="col-lg-6 mx-auto">
            <small class="lead mb-4 sm"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam corporis mollitia tenetur sunt ipsum amet ea earum facilis officiis rem rerum accusantium reprehenderit dicta vel sed, praesentium nemo quod voluptatibus!</small>
        <div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <button type="button" class="btn btn-info btn-lg px-4">Secondary</button>
        </div>
        </div>
    </div>
    </div>
  )
}
