import React from 'react'
import illustration from '../assets/images/illustration-stay-productive.png'

export default function Heros2() {
  return (
    <div className='d-flex justify-content-center'>
        <div class="px-5 py-5 my-5 text-center d-flex flex-wrap justify-content-center">
            <div>
            <img class="d-block mx-auto mb-4" src={illustration} alt="" width="400" height="300"/>
            </div>
            <div class="col-lg-6 mx-auto">
            <h3 class="display-5 fw-bold"  style={{color : 'hsl(0, 0%, 100%)'}}> All you file in one secure</h3>
            <p>
                <small class="lead mb-4 sm"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam corporis mollitia tenetur sunt ipsum amet ea earum facilis officiis rem rerum accusantium reprehenderit dicta vel sed, praesentium nemo quod voluptatibus!</small>
            </p>
            <p>
                <small class="lead mb-4 sm"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam corporis mollitia tenetur sunt ipsum amet ea earum facilis officiis rem rerum accusantium reprehenderit dicta vel sed, praesentium nemo quod voluptatibus!</small>
            </p>
            </div>
        </div>
    </div>
  )
}
