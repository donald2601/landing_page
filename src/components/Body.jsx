import React from 'react'
import './index.css'
import bg from '../assets/images/bg-curvy-mobile.svg'
import Hero from './Hero'
import Card from './Card'
import Heros2 from './Heros2'
import Cards2 from './Cards2'
// import Footer from './Footer'

export default function Body() {
  return (
    <>
    <div className='d-flex flex-column container-fluid' id='body' style={{backgroundImage : {bg}}}>
        <Hero/>
        <Card/>
        <Heros2/>
        <Cards2/>
    </div>
    </>
    
  )
}
